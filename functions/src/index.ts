import { setupNewStreamingSession, startStreamingSession, scheduleStopAndDeleteStreamingSession, stopAndDeleteStreamingSession, getPlayToken, getPlayLink } from "./azure"
import * as functions from 'firebase-functions';
import * as admin from "firebase-admin"
import Stripe from 'stripe';

admin.initializeApp()

const stripe = new Stripe(functions.config().stripe.private_key, { apiVersion: "2020-08-27" });

exports.processSignUp = functions.region('europe-west1').auth.user().onCreate(async (user) => {
  if (await (await admin.auth().listUsers(2)).users.length === 1) {
    const customClaims = {
      admin: true
    }
    // Set custom user claims on this newly created user.
    return admin.auth().setCustomUserClaims(user.uid, customClaims)
      .catch((error: any) => {
        console.log(error);
      });
  }
});

const listAllUsers = async (nextPageToken: string | undefined = undefined): Promise<object[]> => {
  const users = await admin
    .auth()
    .listUsers(1000, nextPageToken)
  const mappedUsers = users.users.map((userRecord) => {
    const user: any = userRecord.toJSON()
    delete user.passwordHash
    delete user.passwordSalt
    return user
  });
  if (users.pageToken) {
    mappedUsers.push(...await listAllUsers(users.pageToken));
  }
  return mappedUsers
};

export const getUsers = functions.region('europe-west1').https.onCall(async (data, context) => {
  if (context.auth?.token.admin !== true) return { code: 400, message: "Only admins can query users data" }
  const users = await listAllUsers()
  return users
})

export const setAdmin = functions.region('europe-west1').https.onCall(async (data, context) => {
  if (context.auth?.token?.admin !== true) return { code: 400, message: "Only admin can set admin permissions to other users" }
  try {
    if (data.state === true) {
      await admin.auth().setCustomUserClaims(data.uid, { admin: true })
    } else {
      await admin.auth().setCustomUserClaims(data.uid, { admin: false })
    }
    return
  } catch (err) {
    functions.logger.error(err);
    return { code: 500 }
  }
});
export const deleteUser = functions.region('europe-west1').https.onCall(async (data, context) => {
  if (context.auth?.token?.admin !== true) return { code: 400, message: "Only admin can set admin permissions to other users" }
  try {
    await admin.auth().deleteUser(data.uid)
    return
  } catch (err) {
    functions.logger.error(err);
    return { code: 500 }
  }
});

// Start writing Firebase Functions
// https://firebase.google.com/docs/functions/typescript
export const createStripeSession = functions.region('europe-west1').https.onCall(async (data, context) => {
  const reqData: { products: string[], success_url: string, cancel_url: string, mail?: string, studentDiscount?: boolean } = data
  functions.logger.debug("Session request: ", reqData);

  if (!!!reqData?.products || !!!reqData?.success_url || !!!reqData?.cancel_url) throw new functions.https.HttpsError('invalid-argument', 'The function must be called with a "products" array, a "success_url" string and a "cancel_url" string.');
  const firebaseProductsReferences: FirebaseFirestore.DocumentReference[] = reqData.products.map((product: string) => admin.firestore().collection('products').doc(product))
  let products: FirebaseFirestore.QueryDocumentSnapshot<FirebaseFirestore.DocumentData> | FirebaseFirestore.DocumentSnapshot<FirebaseFirestore.DocumentData>[]
  if (firebaseProductsReferences.length <= 10) products = await (await admin.firestore().collection("products").where(admin.firestore.FieldPath.documentId(), 'in', firebaseProductsReferences).get()).docs
  else products = await Promise.all(firebaseProductsReferences.map((product: FirebaseFirestore.DocumentReference) => product.get()))

  const user = admin.firestore().collection("users").doc(String(context.auth?.uid))

  // Add an item per each product in the basket
  const stripeFormat = await Promise.all(products.map(async product => {
    const productData = product.data()
    let bundleProducts: FirebaseFirestore.DocumentSnapshot<FirebaseFirestore.DocumentData>[]
    let bundleDescription: string | undefined
    if (productData?.products) {
      bundleProducts = await Promise.all(productData.products.map(async (prodRef: FirebaseFirestore.DocumentReference<FirebaseFirestore.DocumentData>) => prodRef.get()))
      bundleDescription = productData?.type === "bundle" ? bundleProducts?.reduce((prev: string, elm: any, index: number) => prev + "\"" + elm.data().name + "\"" + (index < bundleProducts.length - 1 ? ", " : ""), "") : undefined
    }
    const bundleMetadata = productData?.type === "bundle" ? productData?.products.map((subProduct: any) => subProduct.id) : undefined
    return {
      price_data: {
        currency: 'eur',
        product_data: {
          name: productData?.name,
          description: bundleDescription,
          metadata: {
            bundleContent: JSON.stringify(bundleMetadata),
            self: product.id
          }
        },
        unit_amount: productData?.price * 100,
      },
      quantity: 1
    }
  }))
  if (stripeFormat.reduce((acc, elm) => acc + elm.price_data.unit_amount, 0) === 0 || reqData.studentDiscount) {
    const productsArray = stripeFormat.map((lineItem) => admin.firestore().collection("products").doc(lineItem.price_data.product_data.metadata.self))
    await user.set({ products: admin.firestore.FieldValue.arrayUnion(...productsArray) }, { merge: true })
    return {
      redirect: "/purchase-success"
    }
  }

  const stripeSession: any = {
    payment_method_types: ["card", "sofort", "giropay", "ideal", "eps"],
    line_items: stripeFormat,
    mode: 'payment',
    allow_promotion_codes: true,
    success_url: reqData.success_url,
    cancel_url: reqData.cancel_url,
    metadata: {
      userId: String(context.auth?.uid)
    }
  }


  // Check if our user is already a Stripe customer
  const userData = await user.get()
  if (userData && userData.data()?.stripeId) stripeSession.customer = userData.data()?.stripeId
  else if (!!reqData.mail) stripeSession.customer_email = reqData.mail

  const session = await stripe.checkout.sessions.create(stripeSession);

  // Save customer data to Firebase
  if (session.customer && !userData.data()?.stripeId) await user?.set({ stripedId: session.customer }, { merge: true })
  return { id: session.id }
});

const fulfillOrder = async (data: Stripe.Checkout.Session) => {
  const userId = String(data.metadata?.userId)
  const products = await stripe.checkout.sessions.listLineItems(data.id, {
    limit: 100, expand: ["data.price.product"]
  }).autoPagingToArray({ limit: 1000 })
  functions.logger.debug("Payment validated for: ", userId, products);
  if (!userId) return
  const userDocument = admin.firestore().collection("users").doc(userId)
  const userDocumentData = await userDocument.get()
  if (data.customer && !userDocumentData.data()?.stripeId) await userDocument?.set({ stripeId: data.customer }, { merge: true })
  const productsArray = products.map((lineItem: Stripe.LineItem) => admin.firestore().collection("products").doc((lineItem.price.product as Stripe.Product).metadata.self))
  await userDocument.set({ products: admin.firestore.FieldValue.arrayUnion(productsArray) }, { merge: true })
  // if (userDocumentData.data()?.products) {
  //   await userDocument.set({ products: uniqBy([...userDocumentData.data()?.products, ...productsArray], "id") }, { merge: true })
  // } else {
  //   await userDocument.set({ products: uniqBy([...productsArray], "id") }, { merge: true })
  // }
}

export const stripeWebhook = functions.region('europe-west1').https.onRequest(async (request, response) => {
  if (request.method !== "POST") response.sendStatus(400)
  const payload = request.rawBody;
  const sig: string = request.headers['stripe-signature'] as string;

  let event;

  try {
    event = stripe.webhooks.constructEvent(payload, sig, functions.config().stripe.webhook_secret);
  } catch (err) {
    response.status(400).send(`Webhook Error: ${err.message}`);
    return;
  }
  functions.logger.debug("Received object on stripeWebhook: ", request.body);
  const session = event.data.object as Stripe.Checkout.Session;

  switch (event.type) {
    case "checkout.session.completed": {
      if (session.payment_status === 'paid') {
        await fulfillOrder(session)
      }
      break;
    }
    case 'checkout.session.async_payment_succeeded': {
      await fulfillOrder(session)
      break;
    }
  }
  response.sendStatus(200);
  return;
});


export const Azure = {
  SetupNewStreamingSession: functions.runWith({ timeoutSeconds: 9 * 60 }).region('europe-west1').https.onCall(setupNewStreamingSession),
  StartStreamingSession: functions.runWith({ timeoutSeconds: 9 * 60 }).region('europe-west1').https.onCall(startStreamingSession),
  ScheduleStopAndDeleteStreamingSession: functions.runWith({ timeoutSeconds: 300 }).region('europe-west1').https.onCall(scheduleStopAndDeleteStreamingSession),
  StopAndDeleteStreamingSession: functions.runWith({ timeoutSeconds: 9 * 60 }).region('europe-west1').https.onRequest(stopAndDeleteStreamingSession),
  GetPlayToken: functions.runWith({ timeoutSeconds: 300 }).region('europe-west1').https.onCall(getPlayToken),
  GetPlayLink: functions.runWith({ timeoutSeconds: 300 }).region('europe-west1').https.onCall(getPlayLink)
}