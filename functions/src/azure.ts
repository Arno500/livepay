import * as functions from 'firebase-functions';
import * as admin from "firebase-admin"
const { CloudTasksClient } = require('@google-cloud/tasks');

import * as msRestNodeAuth from "@azure/ms-rest-nodeauth";
import { AzureMediaServices } from "@azure/arm-mediaservices";
import { deburr, snakeCase } from "lodash"

import { randomBytes } from "crypto"
/*eslint-disable*/
import SignJWT from "jose/jwt/sign"

const clientId = functions.config().azure.client_id;
const secret = functions.config().azure.secret;
const tenantId = functions.config().azure.tenant_id;
const subscriptionId = functions.config().azure.subscription_id

const resourceGroup = functions.config().azure.resource_group
const mediaServiceName = functions.config().azure.media_service_name

const streamingEndpointName = "default-plus"
const replayDurationInMinutes = 10

const getAzureCreds = async () => await (await msRestNodeAuth.loginWithServicePrincipalSecretWithAuthResponse(clientId, secret, tenantId)).credentials
const getMediaServicesClient = async () => {
  const client = new AzureMediaServices(await getAzureCreds(), subscriptionId)
  client.longRunningOperationRetryTimeout = 2;
  return client;
}

export const setupNewStreamingSession = async (data: any, context: functions.https.CallableContext) => {
  const reqData: { id: string, lowLatency: boolean } = data
  if (!context.auth?.token?.admin) return { code: 400, message: "Only admins can prepare broadcast sessions" }
  if (!reqData.id || typeof reqData.lowLatency === "undefined") {
    return { code: 400, message: "No ID or LowLatency mode specified" }
  }
  const productDocument = await admin.firestore().collection("products").doc(reqData.id).get()
  if (!!!productDocument) {
    return { code: 400, message: "Invalid product to attach a stream to" }
  }
  const resourceHash = reqData.id.substring(0, 5).toLowerCase()
  const client = await getMediaServicesClient()
  const mediaService = await client.mediaservices.get(resourceGroup, mediaServiceName);
  const streamDocument = admin.firestore().collection("streams").doc(productDocument.id)
  const firestorePromises = []
  let liveName = snakeCase(deburr(productDocument.data()?.name.substring(0, 10))).replace("_", "-") + "-" + resourceHash.toLowerCase()

  if ((await streamDocument.get()).data()?.initialized === true) return


  // Azure contentPolicy (DRM/Encryption + Key)
  const contentPolicyName = liveName + "-policy"
  if (!productDocument.data()?.contentPolicyName) {
    const contentPolicy = await client.contentKeyPolicies.createOrUpdate(resourceGroup, mediaServiceName, contentPolicyName, {
      description: liveName,
      options: [
        {
          configuration: {
            odatatype: "#Microsoft.Media.ContentKeyPolicyClearKeyConfiguration",
          },
          restriction: {
            audience: liveName,
            issuer: "Arnaux & Co",
            odatatype: "#Microsoft.Media.ContentKeyPolicyTokenRestriction",
            restrictionTokenType: "Jwt",
            primaryVerificationKey: {
              odatatype: "#Microsoft.Media.ContentKeyPolicySymmetricTokenKey",
              keyValue: randomBytes(256)
            }
          }
        }
      ]
    })
    if (!contentPolicy) {
      return { code: 500, message: "Couldn't setup contentPolicy on Azure" }
    }
    firestorePromises.push(streamDocument.set({
      contentPolicyName: contentPolicyName,
    }, { merge: true }))
  }


  // Azure LiveEvent (Streaming input + encoding)
  let event;
  if (!productDocument.data()?.liveName) {
    const allowIps = {
      name: "AllowAll",
      address: "0.0.0.0",
      subnetPrefixLength: 0
    }
    event = await client.liveEvents.create(resourceGroup, mediaServiceName, liveName, {
      input: {
        streamingProtocol: "RTMP",
        accessControl: {
          ip: {
            allow: [allowIps]
          }
        }
      },
      preview: {
        accessControl: {
          ip: {
            allow: [allowIps]
          }
        }
      },
      location: mediaService.location,
      streamOptions: reqData.lowLatency ? ["LowLatency"] : [],
      encoding: {
        encodingType: "Premium1080p",
        keyFrameInterval: reqData.lowLatency ? "PT1S" : "PT2S",
        presetName: "Default1080p",
        stretchMode: "AutoSize"
      },
      description: productDocument.data()?.name
    }, {
      autoStart: false
    })
    if (!event) {
      return { code: 500, message: "Couldn't setup event on Azure" }
    }
    firestorePromises.push(streamDocument.set({
      started: false,
      ready: false,
      liveName: liveName,
    }, { merge: true }))
  } else {
    liveName = productDocument.data()?.liveName
  }


  // Azure asset (Storage for below)
  const assetName = liveName + "-storage"
  if (!productDocument.data()?.assetName) {
    const asset = await client.assets.createOrUpdate(resourceGroup, mediaServiceName, assetName, {
      storageEncryptionFormat: "MediaStorageClientEncryption"
    })
    if (!asset) {
      return { code: 500, message: "Couldn't setup asset on Azure" }
    }
    firestorePromises.push(streamDocument.set({
      started: false,
      ready: false,
      assetName: assetName,
    }, { merge: true }))
  }


  // Azure LiveOutput (DVR + Rewind)
  const outputName = liveName + "-output"
  let output
  if (!productDocument.data()?.liveOutputName) {
    output = await client.liveOutputs.create(resourceGroup, mediaServiceName, liveName, outputName, {
      archiveWindowLength: "PT" + replayDurationInMinutes + "M",
      description: productDocument.data()?.name,
      hls: {
        fragmentsPerTsSegment: reqData.lowLatency ? 1 : undefined
      },
      assetName: assetName
    })
    if (!output) {
      return { code: 500, message: "Couldn't setup and link the output to the LiveEvent" }
    }
    firestorePromises.push(streamDocument.set({
      liveOutputName: outputName,
    }, { merge: true }))
  }
  // Azure StreamingLocator (DRM + Link to storage)
  const locatorName = liveName + "-locator"
  let locator
  if (!productDocument.data()?.locatorName) {
    locator = await client.streamingLocators.create(resourceGroup, mediaServiceName, locatorName, {
      assetName: assetName,
      streamingPolicyName: "Predefined_ClearKey",
      defaultContentKeyPolicyName: contentPolicyName
    })
    if (!locator) {
      return { code: 500, message: "Couldn't setup the StreamingLocator on Azure" }
    }
    firestorePromises.push(streamDocument.set({
      locatorName: locatorName,
    }, { merge: true }))
  }

  // Azure StreamingEndpoint (Azure Ingress/Egress/CDN)
  // const endpoint = await client.streamingEndpoints.get(resourceGroup, mediaServiceName, "default")

  // Save live status to a document
  await streamDocument.set({
    initialized: true
  }, { merge: true })

  return { ...await (await streamDocument.get()).data() }
}

export const startStreamingSession = async (data: any, context: functions.https.CallableContext) => {
  const reqData: { id: string } = data
  if (!context.auth?.token?.admin) return { code: 400, message: "Only admins can start broadcast sessions" }
  if (!reqData.id) {
    return { code: 400, message: "No ID specified" }
  }
  const streamReference = admin.firestore().collection("streams").doc(reqData.id)
  const streamDocument = await streamReference.get()
  if (!!!streamDocument) {
    return { code: 400, message: "Invalid product to attach a stream to" }
  }
  const client = await getMediaServicesClient()
  const liveName = streamDocument.data()?.liveName

  if (!streamDocument.data()?.initialized || !liveName) {
    return { code: 400, message: "Event not setup" }
  }

  await client.liveEvents.start(resourceGroup, mediaServiceName, liveName)

  const streamingEndpoint = await client.streamingEndpoints.get(resourceGroup, mediaServiceName, streamingEndpointName)
  if (streamingEndpoint.resourceState === "Stopped") await client.streamingEndpoints.start(resourceGroup, mediaServiceName, streamingEndpointName)

  const startedEvent = await client.liveEvents.get(resourceGroup, mediaServiceName, liveName)

  if (!Array.isArray(startedEvent.input.endpoints) || startedEvent.input.endpoints.length < 1) return { code: 500, message: "Could not get RTMP endpoints for streaming" }

  await streamReference.set({
    started: true,
    rtmpUrl: startedEvent.input.endpoints[0].url
  }, { merge: true })

  await admin.firestore().collection("products").doc(reqData.id).set({ live: true }, { merge: true })

  return
}

export const stopAndDeleteStreamingSession = async (req: functions.https.Request, res: functions.Response) => {
  const reqData: { id: string, uid: string } = req.body
  const user = await admin.auth().getUser(reqData.uid)
  if (!reqData.id || user.customClaims?.admin !== true) {
    functions.logger.error("No ID specified or user invalid", req.body)
    res.status(400).send("No ID specified or user invalid")
    return
  }
  const streamReference = admin.firestore().collection("streams").doc(reqData.id)
  const productDocument = await streamReference.get()
  if (!!!productDocument) {
    functions.logger.error("Invalid product to attach a stream to", req.body)
    res.status(400).send("Invalid product to attach a stream to")
    return
  }
  const client = await getMediaServicesClient()
  const liveName = productDocument.data()?.liveName

  if (!productDocument.data()?.started || !liveName) {
    res.status(200).send("Event not started")
    return
  }

  await client.liveEvents.deleteMethod(resourceGroup, mediaServiceName, liveName)

  const streamingEndpoint = await client.streamingEndpoints.get(resourceGroup, mediaServiceName, streamingEndpointName)
  if (streamingEndpoint.resourceState === "Running") await client.streamingEndpoints.beginStop(resourceGroup, mediaServiceName, streamingEndpointName)
  await client.assets.deleteMethod(resourceGroup, mediaServiceName, productDocument.data()?.assetName)

  await client.contentKeyPolicies.deleteMethod(resourceGroup, mediaServiceName, productDocument.data()?.contentPolicyName)

  await streamReference.set({
    started: false,
    finished: true
  }, { merge: true })
  await admin.firestore().collection("products").doc(reqData.id).set({ live: false, stopping: false }, { merge: true })

  return
}

export const scheduleStopAndDeleteStreamingSession = async (data: any, context: functions.https.CallableContext) => {
  const reqData: { id: string } = data
  if (!context.auth?.token?.admin) return { code: 400, message: "Only admins can stop broadcast sessions" }
  const client = await getMediaServicesClient()
  await client.liveEvents.stop(resourceGroup, mediaServiceName, (await admin.firestore().collection("streams").doc(reqData.id).get()).data()?.liveName, {
    removeOutputsOnStop: true
  })
  const project = JSON.parse(process.env.FIREBASE_CONFIG!).projectId
  const location = 'europe-west1'
  const queue = 'deleteStreamQueue'
  const task = {
    httpRequest: {
      httpMethod: 'POST',
      url: `https://${location}-${project}.cloudfunctions.net/Azure-StopAndDeleteStreamingSession`,
      body: Buffer.from(JSON.stringify({ ...reqData, uid: context.auth?.uid })).toString('base64'),
      headers: {
        'Content-Type': 'application/json',
      },
    },
    scheduleTime: {
      seconds: (Date.now() / 1000) + (replayDurationInMinutes * 60)
    }
  }
  const tasksClient = new CloudTasksClient()
  const queuePath: string = tasksClient.queuePath(project, location, queue)
  await tasksClient.createTask({ parent: queuePath, task })
  await admin.firestore().collection("products").doc(reqData.id).set({ stopping: true }, { merge: true })
  await admin.firestore().collection("streams").doc(reqData.id).set({ stopping: true }, { merge: true })
  return
}

const canWatchStream = (user: { products: FirebaseFirestore.DocumentReference[] }, productId: string): boolean => {
  if (user.products.some((elm: FirebaseFirestore.DocumentReference) => elm.id === productId) === true) return true
  return user.products.some(async (elm: FirebaseFirestore.DocumentReference) => {
    const data = await elm.get()
    if (data.data()?.type === "bundle") {
      return data.data()?.products.some((concerts: FirebaseFirestore.DocumentReference) => concerts.id === productId)
    }
  })
}


export const getPlayToken = async (data: any, context: any) => {
  const reqData: { id: string } = data
  if (!reqData.id) {
    return { result: 400, message: "No ID specified" }
  }
  const userData = await (await admin.firestore().collection("users").doc(context.auth?.uid).get()).data() as any
  const streamDocument = await admin.firestore().collection("streams").doc(reqData.id).get()
  if (!canWatchStream(userData, reqData.id)) return { code: 400, message: "Unauthorized" }
  const client = await getMediaServicesClient()
  const contentPolicy = await client.contentKeyPolicies.getPolicyPropertiesWithSecrets(resourceGroup, mediaServiceName, streamDocument.data()?.contentPolicyName) as any
  const jwt = await new SignJWT({})
    .setAudience(streamDocument.data()?.liveName)
    .setIssuer("Arnaux & Co")
    .setIssuedAt()
    .setExpirationTime("2h")
    .setProtectedHeader({ type: "JWT", alg: "HS256" })
    .sign(contentPolicy.options[0].restriction.primaryVerificationKey.keyValue)
  return { jwt }
}


export const getPlayLink = async (data: any, context: any) => {
  const reqData: { id: string } = data
  if (!reqData.id) {
    return { result: 400, message: "No ID specified" }
  }
  const streamDocument = await admin.firestore().collection("streams").doc(reqData.id).get()
  const streamData = streamDocument.data()
  if (!streamData?.locatorName) return { code: 400, message: "The stream does not have any locator" }
  const client = await getMediaServicesClient()
  const streamingLocator = await client.streamingLocators.listPaths(resourceGroup, mediaServiceName, streamData?.locatorName)
  const streamingEndpoint = await client.streamingEndpoints.get(resourceGroup, mediaServiceName, streamingEndpointName)
  if (!streamingLocator.streamingPaths) return { code: 400, message: "The stream is not ready, yet!" }
  return { streamingPaths: streamingLocator.streamingPaths, streamingHostname: streamingEndpoint.hostName }
}