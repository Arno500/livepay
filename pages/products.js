import React, { useEffect, useState } from 'react';
import Head from 'next/head'

import { firestore } from "utils/firebase"

import { message, List, Card, PageHeader, Button } from 'antd';

import ProductModal from "components/ProductModal"

import styles from '../styles/Products.module.scss'
import { t, currencyFormatter, dateFormatter } from 'utils/localization';
import ProductCard from 'components/ProductCard';

const responsiveListGrid = {
  gutter: 16,
  xs: 1,
  sm: 1,
  md: 2,
  lg: 3,
  xl: 4,
  xxl: 5,
}

const defaultListProps = {
  pagination: {
    pageSize: 10
  },
  grid: responsiveListGrid,

}

const Table = () => {
  const [data, setData] = useState({ products: [], bundles: [] })
  const [loading, setLoading] = useState(true)
  const [modalVisible, setModalVisible] = useState({})
  const fetchData = async () => {
    let products = await firestore.collection("products").get()
    setData({ products: products.docs.filter(elm => elm.data().type === "product"), bundles: products.docs.filter(elm => elm.data().type === "bundle") })
    setLoading(false)
  }
  useEffect(() => fetchData(), [])
  const handleCardClick = (id) => {
    if (!modalVisible[id]) {
      setModalVisible({ [id]: true })
    }
  }
  const updateProduct = async (data, itemId) => {
    try {
      if (itemId) {
        await firestore.collection("products").doc(itemId).set(data)
      } else {
        await firestore.collection("products").add(data)
      }
      await fetchData()
      if (itemId) {
        setModalVisible((prev) => ({ ...prev, [itemId]: false }))
      } else {
        setModalVisible({})
      }
    } catch (err) {
      message.error(err.message)
    }
  }

  const deleteProduct = async (itemId) => {
    try {
      await firestore.collection("products").doc(itemId).delete()
      await fetchData()
      if (itemId) {
        setModalVisible((prev) => ({ ...prev, [itemId]: false }))
      } else {
        setModalVisible({})
      }
    } catch (err) {
      message.error(err.message)
    }
  }
  return (
    <>
      <PageHeader title={t("product.products")} extra={[<Button key="product" type='primary' onClick={() => setModalVisible((prev) => ({ ...prev, product: true }))} >{t("product.createProduct")}</Button>]} />
      <List
        {...defaultListProps}
        dataSource={data.products}
        loading={loading ? { size: "large" } : false}
        renderItem={item => {
          const itemData = item.data()
          return (
            <List.Item>
              <ProductCard item={itemData} onClick={() => handleCardClick(item.id)} showPrice>
                <ProductModal productsList={data.products} product={itemData} sendAction={(data) => updateProduct(data, item.id)} cancelAction={() => setModalVisible({ [item.id]: false })} deleteAction={() => deleteProduct(item.id)} visible={modalVisible[item.id] === true}></ProductModal>
              </ProductCard>
            </List.Item>
          )
        }}
      />
      <ProductModal productsList={data.products} template="product" sendAction={(data) => updateProduct(data)} cancelAction={() => setModalVisible({ product: false })} visible={modalVisible.product === true}></ProductModal>
      <PageHeader title={t("product.bundles")} extra={[<Button type='primary' key="bundle" onClick={() => setModalVisible((prev) => ({ ...prev, bundle: true }))} >{t("product.createBundle")}</Button>]}></PageHeader>
      <List
        {...defaultListProps}
        dataSource={data.bundles}
        loading={loading ? { size: "large" } : false}
        renderItem={item => {
          const itemData = item.data()
          return (
            <List.Item>
              <Card hoverable title={itemData.name} onClick={() => handleCardClick(item.id)}>
                {itemData.description}
                <ProductModal productsList={data.products} product={itemData} sendAction={(data) => updateProduct(data, item.id)} cancelAction={() => setModalVisible({ [item.id]: false })} deleteAction={() => deleteProduct(item.id)} visible={modalVisible[item.id] === true}></ProductModal>
                <Card.Meta className={styles.card} title={currencyFormatter.format(itemData.price)} />
              </Card>
            </List.Item>
          )
        }}
      />
      <ProductModal productsList={data.products} template="bundle" sendAction={(data) => updateProduct(data)} cancelAction={() => setModalVisible({ bundle: false })} visible={modalVisible.bundle === true}></ProductModal>
    </>)
}

export default function Purchase() {
  return (
    <div>
      <Head>
        <title>{t("titles.products")} - {t("app")}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Table />
    </div>
  )
}
