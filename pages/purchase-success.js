import Head from 'next/head'
import Link from "next/link"
import { Button, Result } from 'antd';
import { t } from 'utils/localization';

export default function Streams() {
  return (
    <div>
      <Head>
        <title>{t("titles.purchaseSuccess")} - {t("app")}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Result
        status="success"
        title={t("purchase.success")}
        subTitle={t("purchase.successSub")}
        extra={[
          <Link key="library" href="/library">
            <Button type="primary" key="library">
              {t("purchase.goToLibrary")}
            </Button></Link>,
          <Link key="buy" href="/purchase"><Button>{t("purchase.goBackToPurchases")}</Button></Link>,
        ]}
      />
    </div>
  )
}
