import React, { useEffect, useState } from 'react';
import Head from 'next/head'
import { useRouter } from 'next/router';

import { firestore, functions, auth } from "utils/firebase"

import { useStripe } from '@stripe/react-stripe-js';

import { List, Card, PageHeader, Checkbox, Button, Typography, Drawer, Popconfirm, message } from 'antd';
const { Title, Paragraph } = Typography

import styles from '../styles/Purchase.module.scss'
import { t, currencyFormatter, dateFormatter } from 'utils/localization';
import dayjs from 'dayjs';

const getPrice = (price, nullPrice = null) => {
  if (price > 0) return currencyFormatter.format(price)
  return nullPrice !== null ? "" : t("purchase.free")
}

const responsiveListGrid = {
  gutter: 16,
  xs: 1,
  sm: 1,
  md: 2,
  lg: 3,
  xl: 4,
  xxl: 5,
}

const defaultListProps = {
  pagination: {
    pageSize: 10
  },
  grid: responsiveListGrid,
}

const BuyButton = ({ selectedProducts, bundles, concerts }) => {
  const [loading, setLoading] = useState(false)
  const [visible, setVisible] = useState(false)
  const [studentDiscount, setStudentDiscount] = useState(false)
  const router = useRouter();
  const canBuy = Object.keys(selectedProducts)?.length > 0 && Object.values(selectedProducts)?.some(elm => elm === true)
  const filteredProducts = { ...selectedProducts }
  const stripe = useStripe()
  for (const product in filteredProducts) {
    if (filteredProducts.hasOwnProperty(product)) {
      if (filteredProducts[product] === false) {
        delete filteredProducts[product]
        continue;
      }
      const bundle = bundles.find(bundle => bundle.id === product);
      if (bundle) {
        bundle.data().products.forEach(ref => filteredProducts[ref.id] && delete filteredProducts[ref.id])
      }
    }
  }
  const totalPrice = Object.keys(filteredProducts).reduce((acc, elm) => {
    const inBundle = bundles.find(bundle => bundle.id === elm)
    const inConcert = concerts.find(concert => concert.id === elm)
    if (inBundle) return acc += inBundle.data().price
    if (inConcert) return acc += inConcert.data().price
  }, 0)
  const handleBuy = async () => {
    setLoading(true)
    const userMail = auth().currentUser.email
    const stripeSession = await (await functions.createStripeSession({ products: Object.keys(filteredProducts), success_url: `${window.location.origin}/${router.locale}/purchase-success`, cancel_url: `${window.location.origin}/${router.locale}/purchase`, mail: userMail, studentDiscount })).data
    if (stripeSession.redirect) {
      router.push(stripeSession.redirect)
      return
    }
    const result = await stripe.redirectToCheckout({ sessionId: stripeSession.id })
    if (result.error) {
      message.error(result.error.message);
      setVisible(false)
    }
    setLoading(false)
  }
  return (<Popconfirm
    disabled={!canBuy}
    placement="bottomRight"
    okText={t("purchase.buy")}
    okButtonProps={{ loading: loading }}
    onConfirm={handleBuy}
    onCancel={() => { setVisible(false); setLoading(false) }}
    visible={visible}
    title={
      <>
        <Paragraph>{t("purchase.confirmation")}</Paragraph>
        <List dataSource={Object.keys(filteredProducts)} renderItem={item => {
          const inBundle = bundles.find(elm => elm.id === item)
          const inConcert = concerts.find(elm => elm.id === item)
          if (inBundle) {
            const productsInBundle = inBundle.data().products.map(elm => concerts.find(concert => concert.id === elm.id))
            return (<List.Item extra={[<strong key="0">{getPrice(inBundle.data().price)}</strong>]}>{inBundle.data().name}<br /><ul>{productsInBundle.map(elm => <li key={elm.id}>{elm.data().name}<br /></li>)}</ul><br></br></List.Item>)
          }
          else if (inConcert) return (<List.Item extra={[<strong key="0">{getPrice(inConcert.data().price)}</strong>]}>{inConcert.data().name}</List.Item>)
        }}></List>
        <Title level={3}><List.Item extra={<span>{getPrice(studentDiscount === true ? 0 : totalPrice)}</span>}>{t("purchase.total")}</List.Item></Title>
        <Checkbox onChange={(e) => setStudentDiscount(e.target.checked)} checked={studentDiscount}>{t("purchase.studentDiscount")}</Checkbox>

      </>}
  >
    <Button type="primary" disabled={!canBuy} onClick={() => setVisible(prev => !prev)} >{t("purchase.buy")}</Button>
  </Popconfirm>)
}

const Table = () => {
  const [data, setData] = useState({ products: [], bundles: [] })
  const [loading, setLoading] = useState(true)
  const [selected, setSelected] = useState({})
  const [productDetails, setProductDetails] = useState({})
  const fetchData = async () => {
    let products = await (await firestore.collection("products").get()).docs
    setData({ products: products.filter(elm => elm.data().type === "product"), bundles: products.filter(elm => elm.data().type === "bundle") })
    setLoading(false)
  }
  useEffect(() => fetchData(), [])
  const handleCardClick = (id, disabled) => {
    if (disabled === true) return
    setSelected(prev => ({ ...prev, [id]: !prev[id] }))
  }
  const handleDetails = (id) => {
    setProductDetails(prev => ({ ...prev, [id]: true }))
  }
  return (
    <>
      <PageHeader title={t("purchase.concertPurchase")} extra={[<BuyButton key="0" selectedProducts={selected} bundles={data.bundles} concerts={data.products} />]} />
      <List
        {...defaultListProps}
        dataSource={data.products.filter(elm => elm.data().hidden !== true).sort((a, b) => a.data().date[0] - b.data().date[0])}
        loading={loading ? { size: "large" } : false}
        renderItem={item => {
          const itemData = item.data()
          const duration = dayjs.duration(dayjs(itemData.date[1].toDate()).diff(dayjs(itemData.date[0].toDate()))).humanize()
          return (
            <List.Item>
              <Card hoverable title={itemData.name} onClick={() => handleCardClick(item.id, itemData.hidden)} extra={<Checkbox onClick={(e) => e.preventDefault()} disabled={itemData.hidden} checked={selected[item.id]}></Checkbox>}>
                <Paragraph>{itemData.description}</Paragraph>
                <Title level={4}>
                  {getPrice(itemData.price)}
                </Title>
                <Card.Meta className={styles.card} description={`${dateFormatter.format(itemData.date[0].toDate())} (${duration})`} />
              </Card>
            </List.Item>
          )
        }}
      />
      <PageHeader title={t("purchase.bundlePurchase")}></PageHeader>
      <List
        pagination={{
          pageSize: 5
        }}
        grid={{
          gutter: 16,
          xs: 1,
          sm: 1,
          md: 1,
          lg: 2,
          xl: 3,
          xxl: 4,
        }}
        dataSource={data.bundles.filter(elm => elm.data().hidden !== true)}
        loading={loading ? { size: "large" } : false}
        renderItem={item => {
          const itemData = item.data()
          const productsList = itemData.products.map(product => {
            const productData = data.products.find(elm => elm.id === product.id).data()
            const duration = dayjs.duration(dayjs(productData.date[1].toDate()).diff(dayjs(productData.date[0].toDate()))).humanize()
            return (
              <List.Item actions={[
                <a onClick={() => handleDetails(product.id)} key={`a-${product.id}`}>
                  {t('purchase.details')}
                </a>,
              ]} className={styles.concertInList} onClick={(e) => e.stopPropagation()} key={product.id}>
                <Drawer
                  title={productData.name}
                  placement="right"
                  closable
                  onClose={() => setProductDetails(prev => ({ ...prev, [product.id]: false }))}
                  visible={productDetails[product.id]}
                  width={400}
                >
                  <Paragraph>{productData.description}</Paragraph>
                  <br></br>
                  <Paragraph strong>{dateFormatter.format(productData.date[0].toDate())} ({duration})</Paragraph>
                </Drawer>
                {productData.name}
              </List.Item>)
          })
          const productsTotalPrice = itemData.products.reduce((acc, curr) => acc + data.products.find(elm => elm.id === curr.id).data().price, 0)
          return (
            <List.Item>
              <Card hoverable title={itemData.name} onClick={() => handleCardClick(item.id)} extra={<Checkbox onClick={(e) => e.preventDefault()} checked={selected[item.id]}></Checkbox>}>
                <Paragraph>{itemData.description}</Paragraph>
                <Title level={4}>
                  {getPrice(itemData.price)} {productsTotalPrice < itemData.price && <strike className={styles.oldPrice}>{getPrice(productsTotalPrice, "")}</strike>}
                </Title>
                <br></br>
                {t("purchase.bundleContains")}
                <List
                  itemLayout="horizontal"
                  size="small"
                  bordered
                  className={styles.concertsList}
                >
                  {productsList}
                </List>
              </Card>
            </List.Item>
          )
        }}
      />
    </>)
}

export default function Purchase() {
  return (
    <div>
      <Head>
        <title>{t("titles.purchase")} - {t("app")}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Table />
    </div>
  )
}
