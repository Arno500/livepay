import React, { useEffect, useRef } from 'react';
import Head from 'next/head'
import { useRouter } from 'next/router';
import { t } from 'utils/localization';
import { PageHeader } from 'antd';


export default function Streams() {
  const authContainer = useRef(null);
  const router = useRouter();

  useEffect(() => {
    const firebaseInit = async () => {
      const firebaseui = await import('firebaseui')
      const { auth } = await import('utils/firebase')
      const ui = firebaseui.auth.AuthUI.getInstance() || new firebaseui.auth.AuthUI(auth());
      if (window && authContainer.current) {
        ui.start(authContainer.current, {
          signInSuccessUrl: router.query.redirect || `/${router.locale}/library`,
          signInOptions: [
            // {
            //   provider: auth.PhoneAuthProvider.PROVIDER_ID,
            //   recaptchaParameters: {
            //     size: 'invisible'
            //   },
            //   defaultCountry: 'DE'
            // },
            {
              provider: auth.EmailAuthProvider.PROVIDER_ID,
              requireDisplayName: false
            },
            {
              provider: auth.GoogleAuthProvider.PROVIDER_ID,
              clientId: "799038449274-bt881ua32q1h1pv8rf3jk38ls485glo0.apps.googleusercontent.com"
            }
          ],
          credentialHelper: firebaseui.auth.CredentialHelper.GOOGLE_YOLO,
          tosUrl: "",
          privacyPolicyUrl: ""
        })
      }
    }
    if (window)
      firebaseInit()
  }, [])
  return (
    <div>
      <Head>
        <title>{t("titles.signIn")} - {t("app")}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <PageHeader title={t("titles.signIn")} />
      <div ref={authContainer}></div>
    </div>
  )
}
