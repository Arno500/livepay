import React, { useEffect, useState } from 'react';
import Head from 'next/head'

import { deburr } from "lodash"
import { t } from 'utils/localization';
import { firestore } from 'utils/firebase';

import { PageHeader, List, Input } from 'antd';

import ProductCard from 'components/ProductCard';
import StreamModal from 'components/StreamModal';

export default function Streams() {
  const [loading, setLoading] = useState(true)
  const [modalVisible, setModalVisible] = useState({})
  const [data, setData] = useState([])
  const [filter, setFilter] = useState("")
  const fetchData = async () => {
    const products = await (await firestore.collection("products").where("type", "==", "product").get()).docs
    setData(products)
    setLoading(false)
  }
  useEffect(() => {
    fetchData()
  }, [])
  const handleCardClick = (id) => {
    if (!modalVisible[id]) {
      setModalVisible({ [id]: true })
    }
  }
  return (
    <div>
      <Head>
        <title>{t("titles.streams")} - {t("app")}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <PageHeader title={t("titles.streams")} extra={[<Input allowClear onChange={(e) => setFilter(e.target.value)} key="0" />]} />
      <List
        grid={{
          gutter: 16,
          xs: 1,
          sm: 1,
          md: 2,
          lg: 3,
          xl: 4,
          xxl: 5,
        }}
        pagination={{ pageSize: 20 }}
        dataSource={filter.trim() !== "" ? data.filter(elm => deburr(elm.data().name).toLowerCase().match(deburr(filter.trim()).toLowerCase())) : data}
        loading={loading ? { size: "large" } : false}
        renderItem={item => {
          const itemData = item.data()
          return (
            <List.Item>
              <ProductCard item={itemData} onClick={() => handleCardClick(item.id)} >
                <StreamModal product={itemData} id={item.id} visible={modalVisible[item.id] === true} cancelAction={() => setModalVisible({ [item.id]: false })}></StreamModal>
              </ProductCard>
            </List.Item>
          )
        }}
      />
    </div>
  )
}
