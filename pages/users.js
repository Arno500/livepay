import { useEffect, useState } from 'react'
import Head from 'next/head'

import UserModal from 'components/UserModal'
import ConfirmDelete from "components/ConfirmDelete"

import { t, dateFormatter } from 'utils/localization'
import { firestore, functions } from 'utils/firebase'
import { deburr } from "lodash"

import { Input, PageHeader, Space, Table, Tag } from 'antd'

export default function Users() {
  const [users, setUsers] = useState([])
  const [products, setProducts] = useState([])
  const [filter, setFilter] = useState("")
  const [openModal, setOpenModal] = useState(new Map())
  const [loading, setLoading] = useState(true)
  const getUsers = async () => {
    setLoading(true)
    setUsers(await (await functions.getUsers()).data)
    setLoading(false)
  }
  const getProducts = async () => {
    setProducts((await firestore.collection("products").get()).docs)
  }
  useEffect(() => {
    if (users.length < 1) {
      getUsers()
      getProducts()
    }
  }, [])
  const columns = [
    {
      title: t("users.columns.id"),
      dataIndex: 'email',
      render: (elm, record) => <a href={"mailto:" + elm} title={"Envoyer un mail à " + (record.displayName ?? elm)}>{elm}</a>
    },
    {
      title: t("users.columns.admin"),
      dataIndex: ['customClaims', 'admin'],
      filters: [
        {
          text: t("yes"),
          value: true
        },
        {
          text: t("no"),
          value: false
        }
      ],
      onFilter: (filter, val) => Boolean(val.customClaims?.admin) === filter,
      render: (elm) => elm === true && <Tag color="volcano">{t("users.admin").toUpperCase()}</Tag>
    },
    {
      title: t("users.columns.uid"),
      dataIndex: 'uid'
    },
    {
      title: t("users.columns.creation"),
      dataIndex: ['metadata', 'creationTime'],
      render: (elm) => <>{dateFormatter.format(new Date(elm))}</>,
      sorter: (a, b) => new Date(a.metadata.creationTime) - new Date(b.metadata.creationTime),
    },
    {
      title: t("users.columns.connection"),
      dataIndex: ['metadata', 'lastSignInTime'],
      render: (elm) => <>{dateFormatter.format(new Date(elm))}</>,
      sorter: (a, b) => new Date(a.metadata.lastSignInTime) - new Date(b.metadata.lastSignInTime),
    },
    {
      title: t("users.columns.operations"),
      dataIndex: 'operation',
      render: (_, record) => (
        <Space size="middle">
          <a href="#" onClick={() => openModal.get(record.uid) !== true && setOpenModal(prev => new Map([...prev, [record.uid, true]]))}>
            {t("users.modify")}
            <UserModal products={products} record={record}
              visible={openModal.get(record.uid)}
              onOk={() => setOpenModal(prev => new Map([...prev, [record.uid, false]]))}
              onCancel={() => setOpenModal(prev => new Map([...prev, [record.uid, false]]))} />
          </a>
          <ConfirmDelete uid={record.uid} afterDelete={() => setUsers((prev) => prev.filter(elm => elm.uid !== record.uid))} />
        </Space>
      )
    },
  ]
  return (
    <div>
      <Head>
        <title>{t("titles.users")} - {t("app")}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <PageHeader title={t("titles.users")} extra={<Input allowClear value={filter} onChange={(e) => setFilter(e.target.value)} />} />
      <Table loading={loading} filtered={filter} columns={columns} rowKey="uid" dataSource={filter.trim() !== "" ? users.filter(elm => deburr(elm.email).toLowerCase().match(deburr(filter.trim()).toLowerCase())) : users} />
    </div>
  )
}
