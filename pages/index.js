import Head from 'next/head'
import Link from "next/link"

// import styles from '../styles/Home.module.scss'
import { t } from 'utils/localization';
import { ShoppingCartOutlined, VideoCameraOutlined } from '@ant-design/icons';

import { Button, Typography, Space } from 'antd';

const { Title, Paragraph } = Typography

export default function Home() {
  return (
    <div>
      <Head>
        <title>{t("app")}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Title>{t("home.title")}</Title>
      <Paragraph>{t("home.paragraph")}</Paragraph>
      <ol>
        <Space direction="vertical" size="large">
          <li>{t("home.1")}<br /><Link href="/purchase"><a><Button type="primary" icon={<ShoppingCartOutlined />}>{t("menu.purchase")}</Button></a></Link></li>
          <li>{t("home.2")}<br /><Link href="/library"><a><Button type="primary" icon={<VideoCameraOutlined />}>{t("menu.myConcerts")}</Button></a></Link></li>
        </Space>
      </ol>
    </div>
  )
}
