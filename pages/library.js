import React, { useEffect, useState } from 'react';
import Head from 'next/head'
import Link from 'next/link';
import ProductCard from 'components/ProductCard';

import styles from '../styles/Home.module.scss'

import { t } from 'utils/localization';
import { firestore } from 'utils/firebase';
import { uniqBy, flatten } from "lodash"

import { PageHeader, List, ConfigProvider, Empty, Button, Badge, Divider } from 'antd';

export default function Library({ user }) {
  const [loading, setLoading] = useState(true)
  const [data, setData] = useState([])
  const fetchData = async (load = false) => {
    if (user === null || !user.uid) {
      setData([])
      setLoading(false)
      return
    }
    if (load)
      setLoading(true)
    const myProducts = await (await firestore.collection("users").doc(user.uid).get()).data()?.products
    if (!myProducts) {
      setData([])
      setLoading(false)
      return
    }
    const products = await (await firestore.collection("products").get()).docs.filter(product => myProducts.some((myProduct) => myProduct.id === product.id))
    const productsWithBundleProducts = await Promise.all(products.map(async product => product.data().type === "bundle" ? Promise.all(product.data().products.map(async subProduct => await subProduct.get())) : (async () => product)()))
    setData(uniqBy(flatten(productsWithBundleProducts), "id"))
    setLoading(false)
  }
  useEffect(() => {
    fetchData(true)
    const interval = setInterval(fetchData, 10000)
    return () => clearInterval(interval)
  }, [user])
  const lives = data.filter(elm => elm.data().live === true && elm.data().stopping !== true)
  const nowTime = Date.now()
  const offline = data.filter(elm => (!elm.data().live || elm.data().stopping === true) && (elm.data().date[1].toMillis() + 1 * 60 * 60 * 1000 >= nowTime)).sort((a, b) => a.data().date[0].toMillis() > b.data().date[1].toMillis())
  return (
    <div>
      <Head>
        <title>{t("titles.library")} - {t("app")}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <PageHeader title={t("library.myItems")} />
      <ConfigProvider renderEmpty={() => <p>{t("library.noRunningLive")}</p>}>
        <List
          grid={{
            gutter: 16,
            xs: 1,
            sm: 1,
            md: 2,
            lg: 3,
            xl: 4,
            xxl: 5,
          }}
          dataSource={lives}
          renderItem={item => {
            const itemData = item.data()
            return (
              <List.Item key={item.id}>
                <Badge.Ribbon color="red" text={t("library.live")}>
                  <Link href={`/live/${encodeURIComponent(item.id)}`}>
                    <a>
                      <ProductCard item={itemData} />
                    </a>
                  </Link>
                </Badge.Ribbon>
              </List.Item>
            )
          }}
        />
      </ConfigProvider>
      <Divider />
      <PageHeader title={t("library.toCome")} />
      <ConfigProvider renderEmpty={() => <Empty
        description={
          <span>
            {t("library.noConcerts")}
          </span>
        }
      >
        <Link href="/purchase">
          <Button type="primary">{t("library.exploreOther")}</Button>
        </Link>
      </Empty>}>

        <List
          grid={{
            gutter: 16,
            xs: 1,
            sm: 1,
            md: 2,
            lg: 3,
            xl: 4,
            xxl: 5,
          }}
          pagination={{ pageSize: 20 }}
          dataSource={offline}
          loading={loading ? { size: "large" } : false}
          renderItem={item => {
            const itemData = item.data()
            return (
              <List.Item key={item.id}>
                <ProductCard item={itemData} hoverable={false} />
              </List.Item>
            )
          }}
        />
      </ConfigProvider>
    </div>
  )
}
