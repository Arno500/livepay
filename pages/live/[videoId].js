import React, { useEffect, useRef, useState } from 'react'
import { useRouter } from 'next/router'
import { firestore, functions } from 'utils/firebase'
import { t } from 'utils/localization'

import { Breadcrumb, Col, Divider, Progress, Row, Space } from 'antd'
import { VideoCameraOutlined } from '@ant-design/icons';
import { AzureMP } from 'react-azure-mp'


import styles from "styles/Live.module.scss"
import Head from 'next/head'

export default function Live() {
  const router = useRouter()
  const token = useRef(null)
  const [liveName, setLiveName] = useState("")
  const [srcData, setSrcData] = useState(null)
  const [progressData, setProgressData] = useState({ text: t("live.loading.preparing"), progress: 20 })
  const azurePlayer = useRef()
  const getToken = async (videoId) => {
    try {
      const tokenResponse = await (await functions.azure.GetPlayToken({ id: videoId })).data
      if (tokenResponse && tokenResponse.code === 400) router.back()
      token.current = tokenResponse.jwt
      return tokenResponse.jwt
    } catch (err) {
      router.replace("/library")
    }
  }
  const getLinks = async (videoId) => {
    const linkResponse = await (await functions.azure.GetPlayLink({ id: videoId })).data
    if (linkResponse.streamingPaths[0].paths.length < 1) {
      setProgressData({ text: t("live.loading.waiting"), progress: 95 })
      setTimeout(() => getLinks(videoId), 5000)
      return
    }
    const sources = linkResponse.streamingPaths.map(endpoint => {
      const result = {
        src: `https://${linkResponse.streamingHostname}${endpoint.paths[0]}`
      }
      switch (endpoint.streamingProtocol) {
        case "Hls":
          result.type = "application/x-mpegURL"
          const url = new URL(process.env.NEXT_PUBLIC_AZURE_HLS_MANIFEST_PROXY)
          url.searchParams.set("token", token.current)
          url.searchParams.set("playbackUrl", `https://${linkResponse.streamingHostname}${endpoint.paths[0]}`)
          result.src = url.toString()
          break
        case "Dash":
          result.type = "application/dash+xml"
          result.protectionInfo = [{
            type: "AES",
            authenticationToken: "Bearer=" + token.current
          }]
          break
      }
      return result
    })
    setSrcData(sources)
  }
  const initSrc = async (videoId) => {
    setProgressData({ text: t("live.loading.checkRights"), progress: 50 })
    await getToken(videoId)
    setProgressData({ text: t("live.loading.streamUrls"), progress: 80 })
    await getLinks(videoId)
  }
  useEffect(() => {
    if (!router.query.videoId || router.query.videoId === "[videoId]") return
    (async () => setLiveName(await (await firestore.collection("products").doc(router.query.videoId).get()).data()?.name))()
    initSrc(router.query.videoId)
    return () => {
      if (!(process.env.NODE_ENV !== "production")) {
      }
    }
  }, [router.query.videoId])
  const setupPlayer = (player) => {
    azurePlayer.current = player
    player.addEventListener("error", () => {
      const code = player.error().code
      if ((code & amp.errorCode.networkErrHttpUserAuthRequired)
        || (code & amp.errorCode.networkErrHttpNotAllowed)
        || (code & amp.errorCode.networkErrHttpUserForbidden)
      ) {
        setTimeout(() => initSrc(router.query.videoId), 5000)
      }
    })
  }
  return (
    <>
      <Head>
        <title>{t("titles.broadcast", { name: liveName })} - {t("app")}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Breadcrumb>
        <Breadcrumb.Item href="/library">
          <VideoCameraOutlined />
          <span>{t("menu.myConcerts")}</span>
        </Breadcrumb.Item>
        <Breadcrumb.Item>{liveName}</Breadcrumb.Item>
      </Breadcrumb>
      {srcData && <div className={styles.videoWrapper}><AzureMP className={styles.player} onInstanceCreated={setupPlayer} options={{ fluid: false, autoplay: true, wallClockTimeDisplaySettings: { enabled: true, useLocalTimeZone: true, controlBar12HourFormat: false }, heuristicProfile: "HighQuality" }} tabindex={0} src={srcData ? srcData : undefined}></AzureMP></div>}
      {!srcData &&
        <Row align="middle" className={styles.row} justify="center">
          <Col span={12}>
            <Space direction="vertical" className={styles.spacer} split={<Divider className={styles.divider} />}>
              <Progress percent={progressData.progress} status="active" showInfo={false} className={styles.progress} />
              <span>{progressData && progressData.text}</span>
            </Space>
          </Col>
        </Row>}
    </>
  )
}
