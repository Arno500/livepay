import React from 'react';
import { useRouter } from 'next/router';

import Layout from "components/Layout";
import '../styles/globals.scss'

import { I18nextProvider } from 'react-i18next';
import i18n, { antdIntlMap } from 'utils/localization'
import dayjs from "dayjs"

import { ConfigProvider } from 'antd';

import { loadStripe } from '@stripe/stripe-js/pure';
import { Elements } from '@stripe/react-stripe-js';

const stripe = loadStripe(process.env.NEXT_PUBLIC_STRIPE_PUBLIC_KEY)

function MyApp({ Component, pageProps }) {
  const router = useRouter()
  if (router.locale) {
    i18n.changeLanguage(router.locale)
    dayjs.locale(router.locale.split("-")[0])
  }
  return (
    <I18nextProvider i18n={i18n}>
      <Elements stripe={stripe}>
        <ConfigProvider locale={i18n.language.split("-")[0] ? antdIntlMap[i18n.language.split("-")[0]] : antdIntlMap.en}>
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </ConfigProvider>
      </Elements>
    </I18nextProvider>
  )
}

export default MyApp
