import React, { useEffect, useRef, useState } from 'react'
import dayjs from "dayjs"

import { firestore } from "utils/firebase"

import { Modal, Button, Form, Input, InputNumber, DatePicker, Radio, Select, Popconfirm, Checkbox } from 'antd';

import { t, currencyFormatter, dateFormatter } from 'utils/localization';

export default function ProductModal({ visible, product, template, sendAction, cancelAction, deleteAction, productsList }) {
  const [loading, setLoading] = useState(false)
  const [deleteLoading, setDeleteLoading] = useState(false)
  const form = useRef()
  const handleOK = async () => {
    if (loading || deleteLoading) return
    try {
      await form.current.validateFields()
    } catch (err) {
      return
    }
    setLoading(true)
    const formData = form.current.getFieldsValue()
    if (formData.date)
      formData.date = formData.date.map(date => date.toDate())
    if (formData.products)
      formData.products = formData.products.map(elm => firestore.collection("products").doc(elm))
    const promise = sendAction(formData)
    if (promise) {
      promise.then(() => setLoading(false))
    }
  }
  const handleDelete = async () => {
    if (loading || deleteLoading) return
    setDeleteLoading(true)
    const promise = deleteAction(product)
    if (promise) {
      promise.then(() => setDeleteLoading(false))
    }
  }
  const handleTypeChange = (evt) => {
    setType(evt.target.value)
  }
  const [date, setDate] = useState()
  const [type, setType] = useState(product?.type ?? template)
  useEffect(() => product?.date && setDate([dayjs(product.date[0].toDate()), dayjs(product.date[1].toDate())]), [product?.date])
  const templateTitles = {
    product: t("product.createProduct"),
    bundle: t("product.createBundle")
  }
  const title = product ? t("product.modify", { name: product.name }) : templateTitles[template]
  const products = product?.products?.map(elm => elm.id)
  // if (product && !!!product.hidden) product.hidden = false
  return (
    <>
      <Modal
        visible={visible}
        title={title}
        onOk={handleOK}
        onCancel={cancelAction}
        width={700}
        footer={[
          product && <Popconfirm key="delete" title={t("product.deleteConfirmation")} okButtonProps={{ danger: true }} onConfirm={handleDelete}><Button loading={deleteLoading} danger>{t("general.delete")}</Button></Popconfirm>,
          <Button key="back" onClick={cancelAction}>
            {t("general.cancel")}
          </Button>,
          <Button key="submit" type="primary" loading={loading} onClick={handleOK}>
            {t("general.submit")}
          </Button>,
        ]}
        destroyOnClose
      >
        <Form
          name="product"
          labelCol={{ span: 5 }}
          initialValues={{ ...product, date, type, products, hidden: product?.hidden ? product.hidden : false }}
          onFinish={handleOK}
          ref={form}
        // onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label={t("product.type")}
            name="type"
            rules={[{ required: true, message: t("product.typeRequired") }]}
          >
            <Radio.Group onChange={handleTypeChange}>
              <Radio.Button value="product">{t("product.product")}</Radio.Button>
              <Radio.Button value="bundle">{t("product.bundle")}</Radio.Button>
            </Radio.Group>
          </Form.Item>
          <Form.Item
            label={t("product.name")}
            name="name"
            rules={[{ required: true, message: t("product.nameRequired") }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label={t("product.description")}
            name="description"
            rules={[{ required: true, message: t("product.descriptionRequired") }]}
          >
            <Input.TextArea allowClear autoSize={{ minRows: 2, maxRows: 8 }} />
          </Form.Item>
          <Form.Item
            label={t("product.price")}
            name="price"
            rules={[{ required: true, message: t("product.priceRequired") }]}

          >
            <InputNumber min={0} step={0.01} formatter={value => `${value}€`} parser={value => value.replace('€', '')} />
          </Form.Item>
          {
            type === "product" &&
            <Form.Item
              label={t("product.date")}
              name="date"
              rules={[{ required: true, message: t("product.dateRequired") }]}
            >
              <DatePicker.RangePicker
                showTime
                format={"ddd DD MMM YYYY HH:mm"}
              // format="lll" for when Antd will debug their DayJS integration
              />
            </Form.Item>
          }
          {type === "bundle" &&
            <Form.Item
              label={t("product.products")}
              name="products"
            >
              <Select
                allowClear
                mode="multiple"
                placeholder={t("product.productSelect")}
                optionLabelProp="label"
                optionFilterProp="label"
                size="large"
              >
                {productsList.map(elm => {
                  const data = elm.data()
                  return (
                    <Select.Option value={elm.id} key={elm.id} label={data.name}>
                      <div>
                        <strong>{data.name}</strong> <i>({dateFormatter.format(data.date[0].toDate())})</i> - {currencyFormatter.format(data.price)}
                      </div>
                    </Select.Option>)
                })}

              </Select>
            </Form.Item>
          }
          <Form.Item
            label={t("product.hidden")}
            name="hidden"
            valuePropName="checked"
          >
            <Checkbox />
          </Form.Item>

        </Form>
      </Modal>
    </>
  );
}