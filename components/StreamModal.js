import React, { useEffect, useState } from 'react'

import { firestore, functions } from "utils/firebase"

import {
  Button, Descriptions, Modal, Popconfirm, Space, Spin, Timeline
} from 'antd';

import { CheckOutlined, LoadingOutlined } from "@ant-design/icons"

import { t } from 'utils/localization';

import styles from "./StreamModal.module.scss"

const StartedStreamWidget = ({ url, stopping, children, onClick, ...props }) => {
  return <>
    <Descriptions title={t("stream.OBSSettings")} className={stopping === true ? styles.stoppingStreamSettings : ""} column={1} bordered size="small">
      <Descriptions.Item className={styles.streamingWidgetData} label={<p className={styles.streamingWidgetLabel}>{t("stream.url")}</p>}>{url}</Descriptions.Item>
      <Descriptions.Item label={t("stream.key")}>bestconcertsever</Descriptions.Item>
    </Descriptions>
    <br></br>
    {stopping === true
      ?
      <span><Space><Spin indicator={<LoadingOutlined />} /> <b>{t("stream.stopping")}</b></Space></span>
      :
      <Popconfirm onConfirm={onClick} title={t("stream.stopConfirmation")}>
        <Button type="primary" danger {...props}>{children}</Button>
      </Popconfirm>
    }
  </>
}

export default function StreamModal({ visible, product, cancelAction, id }) {
  const title = t("stream.broadcast", { name: product.name })
  const [loadingStream, setLoadingStream] = useState(true)
  const [stream, setStream] = useState(false)
  const [buttonLoading, setButtonLoading] = useState(false)
  const fetchStream = async () => {
    setStream(await (await firestore.collection("streams").doc(id).get()).data())
    setLoadingStream(false)
  }
  useEffect(() => {
    fetchStream()
  }, [])
  const prepareStream = async () => {
    setButtonLoading(true)
    await functions.azure.SetupNewStreamingSession({ id, lowLatency: false })
    setButtonLoading(false)
    fetchStream()
  }
  const startStream = async () => {
    setButtonLoading(true)
    await functions.azure.StartStreamingSession({ id })
    setButtonLoading(false)
    fetchStream()
  }
  const stopStream = async () => {
    setButtonLoading(true)
    await functions.azure.StopAndDeleteStreamingSession({ id })
    setButtonLoading(false)
    fetchStream()
  }
  const resetStream = async () => {
    await firestore.collection("streams").doc(id).set({})
    setStream(undefined)
  }
  return (
    <>
      <Modal
        visible={visible}
        title={title}
        onCancel={cancelAction}
        footer={null}
        width={700}
        destroyOnClose
      >
        {
          !loadingStream ?
            <Timeline pending={stream?.started ? <StartedStreamWidget stopping={stream.stopping} url={stream.rtmpUrl} onClick={stopStream} loading={buttonLoading}>{t("stream.stop")}</StartedStreamWidget> : undefined}>
              <Timeline.Item color={stream?.initialized ? undefined : "gray"}>{t("stream.prepared")} {!stream?.initialized ? <Button type="primary" className={styles.actionButton} onClick={prepareStream} loading={buttonLoading}>{t("stream.prepare")}</Button> : null}</Timeline.Item>
              <Timeline.Item color={stream?.started || stream?.finished ? undefined : "gray"}>{t("stream.started")} {stream?.initialized && !stream?.started && !stream?.finished ? <Popconfirm
                title={t("stream.billingStarting")}
                onConfirm={startStream}
              >
                <Button type="primary" className={styles.actionButton} loading={buttonLoading}>{t("stream.start")}</Button>
              </Popconfirm>
                : null}</Timeline.Item>
              {stream?.finished &&
                <Timeline.Item dot={<CheckOutlined className={styles.timelineIcon} />} color={stream?.finished ? "green" : "gray"}>{t("stream.ended")} <Button type="ghost" className={styles.actionButton} onClick={resetStream} loading={buttonLoading}>{t("stream.reset")}</Button></Timeline.Item>
              }
            </Timeline>
            :
            <Spin size="large" />
        }
      </Modal>
    </>
  );
}