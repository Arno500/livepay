import React from 'react'
import { Card } from 'antd'
import { currencyFormatter, dateFormatter } from 'utils/localization'

import styles from "./ProductCard.module.scss"

export default function ProductCard({ children, item, hoverable, showPrice, ...props }) {
  return (
    <Card hoverable={hoverable !== undefined ? hoverable : true} title={item.name} {...props}>
      {item.description}
      {children}
      <Card.Meta className={styles.card} title={showPrice ? currencyFormatter.format(item.price) : undefined} description={dateFormatter.format(item.date[0].toDate())} />
    </Card>
  )
}
