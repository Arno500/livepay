import React, { useState } from 'react'

import { Popconfirm } from 'antd'

import { functions } from 'utils/firebase'
import { t } from 'utils/localization'

export default function ConfirmDelete({ uid, afterDelete }) {
  const [loading, setLoading] = useState(false)
  const [visible, setVisible] = useState(false)
  const handleConfirm = async () => {
    setLoading(true)
    await functions.deleteUser({ uid })
    setLoading(false)
    setVisible(false)
    afterDelete()
  }
  return (
    <Popconfirm placement="topRight" destroyTooltipOnHide visible={visible} onConfirm={handleConfirm} onCancel={() => setVisible(false)} okButtonProps={{ loading }} title={t("users.confirmDelete")}>
      <a onClick={() => setVisible(true)} href="#" style={{ color: "red" }}>{t("users.delete")}</a>
    </Popconfirm>
  )
}
