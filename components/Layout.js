import React, { useState, useRef, useEffect } from 'react'
import { useRouter } from 'next/router'
import Link from 'next/link'

import { t } from 'utils/localization'

import URL from "url"
import { getRouteConfig } from "utils/routeConfig"

import styles from "./Layout.module.scss";

import 'firebaseui/dist/firebaseui.css'

import { Layout, Menu, Typography, BackTop } from 'antd';
import { HomeOutlined, ShoppingCartOutlined, PlayCircleOutlined, HddOutlined, SettingOutlined, VideoCameraOutlined, UserOutlined, ShopOutlined, UserDeleteOutlined, UserAddOutlined } from '@ant-design/icons';

const { Header, Footer, Sider, Content } = Layout;
const { Text } = Typography

export default function AppLayout({ children, ...props }) {
    const [collapsed, setCollapsed] = useState(false);
    const router = useRouter();
    const user = useRef(null);
    const [userStatus, setUserStatus] = useState(null);
    const pageLoaded = useRef(false)
    const authRouteHandler = (url) => {
        if (pageLoaded.current === false) return
        const urlObject = URL.parse(url)
        const splittedPath = urlObject.pathname.split("/")
        const routerUrl = splittedPath[1] === "" ? "" : "/" + [...splittedPath].splice(2).join("/")
        const parsedUrl = getRouteConfig(routerUrl)
        if (parsedUrl?.signedIn === true && !!!user.current) {
            router.push({ pathname: "/sign-in", query: { "redirect": `${window.location.origin}/${urlObject.pathname.substring(1)}` } })
            return false;
        } else if (parsedUrl?.admin === true && !!!user.current?.claims.admin !== true) {
            return false;
        } else {
            return true;
        }
    }
    const signOut = async () => {
        const { auth } = await import('utils/firebase')
        await auth().signOut()
        user.current = null
        pageLoaded.current = true
    }
    useEffect(() => {
        const firebaseInit = async () => {
            const { auth } = await import('utils/firebase')
            auth().onAuthStateChanged(async (authedUser) => {
                pageLoaded.current = true
                if (authedUser) {
                    const idToken = await authedUser.getIdTokenResult()
                    authedUser.claims = { ...idToken.claims }
                    user.current = authedUser
                    setUserStatus(authedUser)
                } else {
                    user.current = null
                    setUserStatus(null)
                    const parsedUrl = getRouteConfig(router.pathname)
                    if (parsedUrl?.signedIn === true || parsedUrl?.admin === true) {
                        router.push("/")
                    }
                }
            })
        }
        if (window)
            firebaseInit()
        router.events.on('routeChangeComplete', authRouteHandler);
        router.beforePopState(authRouteHandler)
    }, [])

    return (
        <Layout className={styles.layoutWrapper} hasSider>
            <Sider collapsible collapsed={collapsed} onCollapse={() => setCollapsed((prev) => !prev)}>
                <div className="logo" />
                <Menu theme="dark" defaultSelectedKeys={[router.pathname || '/library']} selectedKeys={router.pathname !== "/" ? router.pathname : "home"} defaultOpenKeys={['/concerts']} mode="inline" >
                    <Menu.Item key="home" icon={<HomeOutlined />} >
                        <Link href="/">
                            {t("menu.home")}
                        </Link>
                    </Menu.Item>
                    <Menu.SubMenu key="/concerts" title={t("menu.concerts")} icon={<PlayCircleOutlined />} >
                        <Menu.Item key="/library" icon={<VideoCameraOutlined />} >
                            <Link href="/library">
                                {t("menu.myConcerts")}
                            </Link>
                        </Menu.Item>
                        <Menu.Item key="/purchase" icon={<ShoppingCartOutlined />}>
                            <Link href="/purchase">
                                {t("menu.purchase")}
                            </Link>
                        </Menu.Item>
                    </Menu.SubMenu>
                    {userStatus?.claims?.admin === true &&
                        <Menu.SubMenu key="admin" title={t("menu.admin")} icon={<SettingOutlined />} >
                            <Menu.Item key="/streams" icon={<HddOutlined />} >
                                <Link href="/streams">
                                    {t("menu.broadcasts")}
                                </Link>
                            </Menu.Item>
                            <Menu.Item key="/products" icon={<ShopOutlined />}>
                                <Link href="/products">
                                    {t("menu.products")}
                                </Link>
                            </Menu.Item>
                            <Menu.Item key="/users" icon={<UserOutlined />}>
                                <Link href="/users">
                                    {t("menu.users")}
                                </Link>
                            </Menu.Item>
                        </Menu.SubMenu>
                    }
                    {userStatus ?
                        <Menu.Item key="disconnect" icon={<UserDeleteOutlined />} selectable={false} onClick={signOut}>
                            {t("menu.disconnect")}
                        </Menu.Item>
                        :
                        <Menu.Item key="connect" icon={<UserAddOutlined />} selectable={false} >
                            <Link href="/sign-in">
                                {t("menu.connect")}
                            </Link>
                        </Menu.Item>
                    }
                </Menu>
            </Sider>
            <Layout className="site-layout">
                <Header className="site-layout-background" style={{ padding: 0 }} />
                <Content style={{ margin: '0 16px' }}>
                    <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
                        {React.cloneElement(children, { user: user.current, ...props })}
                    </div>
                </Content>
                <Footer style={{ textAlign: 'center' }}><Text type="secondary">Arnaux & Co {new Date().getFullYear()}</Text></Footer>
            </Layout>
            <BackTop />
        </Layout>
    )
}
