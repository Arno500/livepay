import React, { useEffect, useState } from "react"

import { firestore, functions } from 'utils/firebase'
import { currencyFormatter, t } from "utils/localization"

import { Modal, Space, Select, Checkbox, Tag, Spin } from "antd"

export default function UserModal({ products, record, visible, onOk, onCancel, ...props }) {
  const [userData, setUserData] = useState()
  const [selected, setSelected] = useState([])
  const [admin, setAdmin] = useState(record.customClaims?.admin)
  const [loading, setLoading] = useState(true)
  const [saveLoading, setSaveLoading] = useState(false)
  const fetchUserFromFirebase = async () => {
    const fetched = await firestore.collection("users").doc(record.uid).get()
    setUserData(fetched)
    setSelected(fetched.data()?.products?.map(elm => elm.id))
    setLoading(false)
  }
  const sendData = async (...args) => {
    setSaveLoading(true)
    await firestore.collection("users").doc(record.uid).set({ products: selected.map(elm => firestore.collection("products").doc(elm)) }, { merge: true })
    if (record.customClaims?.admin !== admin) {
      console.warn("Changin admin state of " + record.uid + " " + record.email)
      if (record.customClaims) record.customClaims.admin = admin
      else record.customClaims = { admin: admin }
      await functions.setAdmin({ uid: record.uid, state: admin })
    }
    setSaveLoading(false)
    setSelected([])
    setLoading(true)
    onOk(...args)
  }
  useEffect(() => {
    if (visible !== true) return
    fetchUserFromFirebase()
  }, [visible])
  return (
    <Modal
      title={t("users.modifyUser", { user: record.email })}
      destroyOnClose
      visible={visible}
      onOk={sendData}
      onCancel={onCancel}
      okButtonProps={{ loading: saveLoading }}
    >
      {loading ? <Spin size="large" /> :
        <Space direction="vertical" size="large" style={{ width: "100%" }}>
          <Select
            mode="multiple"
            placeholder={t("product.productSelect")}
            optionLabelProp="label"
            optionFilterProp="label"
            size="large"
            style={{ width: "100%" }}
            defaultValue={Array.isArray(userData?.data()?.products) ? userData?.data()?.products.map((elm) => elm.id) : []}
            onChange={(val) => setSelected(val)}
          >
            {products.map(elm => {
              const data = elm.data()
              return (
                <Select.Option color={data.type === "product" ? "geekblue" : "orange"} value={elm.id} key={elm.id} label={data.name}>
                  <div>
                    <Tag color={data.type === "product" ? "geekblue" : "orange"}>{t("product." + data.type)}</Tag> <strong>{data.name}</strong> - {currencyFormatter.format(data.price)}
                  </div>
                </Select.Option>)
            })}
          </Select>
          <Checkbox defaultChecked={record.customClaims?.admin} onChange={(e) => setAdmin(e.target.checked)}>{t("users.administrator")}</Checkbox>
        </Space>
      }
    </Modal >
  )
}