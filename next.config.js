/* eslint-disable */
// const withLess = require('@zeit/next-less');
const withPluginAntd = require("next-plugin-antd-less")

const AntdDayjsWebpackPlugin = require("antd-dayjs-webpack-plugin");

const withBundleAnalyzer = require('@next/bundle-analyzer')({
    enabled: process.env.ANALYZE === 'true',
})

const plugins = [
    // withLess({
    //     lessLoaderOptions: {
    //         javascriptEnabled: true,
    //         modifyVars: {
    //             // ["primary-color"]: "#32a852"
    //         }, // make your antd custom effective
    //         importLoaders: 0
    //     },
    //     cssLoaderOptions: {
    //         importLoaders: 3,
    //         localIdentName: '[local]___[hash:base64:5]'
    //     },
    //     webpack: (config, { isServer }) => {
    //         if (isServer) {
    //             const antStyles = /antd\/.*?\/style.*?/;
    //             const origExternals = [...config.externals];
    //             config.externals = [
    //                 (context, request, callback) => {
    //                     if (request.match(antStyles)) return callback();
    //                     if (typeof origExternals[0] === 'function') {
    //                         origExternals[0](context, request, callback);
    //                     } else {
    //                         callback();
    //                     }
    //                 },
    //                 ...(typeof origExternals[0] === 'function' ? [] : origExternals)
    //             ];

    //             config.module.rules.unshift({
    //                 test: antStyles,
    //                 use: 'null-loader'
    //             });
    //         }
    //         config.plugins.push(new AntdDayjsWebpackPlugin())
    //         return config;
    //     }
    // }),
]
// module.exports = withSass({
//     cssModules: true,
//     ...withPluginAntd({
//         lessLoaderOptions: {
//             javascriptEnabled: true
//         },
//         ...withCss()
//     })
// });
module.exports = withBundleAnalyzer(withPluginAntd({
    i18n: {
        // These are all the locales you want to support in
        // your application
        locales: ['de', 'fr', 'en'],
        // This is the default locale you want to be used when visiting
        // a non-locale prefixed path e.g. `/hello`
        defaultLocale: 'en'
    },
    cssModules: true,
    webpack: (config) => {
        config.plugins.push(new AntdDayjsWebpackPlugin())
        return config;
    }
}));