import firebase from 'firebase';

const config = {
  apiKey: process.env.NEXT_PUBLIC_FIREBASE_API_KEY,
  appId: process.env.NEXT_PUBLIC_APP_ID,
  projectId: process.env.NEXT_PUBLIC_PROJECT_ID,
  authDomain: process.env.NEXT_PUBLIC_AUTH_DOMAIN,
  databaseURL: process.env.NEXT_PUBLIC_DB_URL
};
if (firebase.apps.length === 0)
  firebase.initializeApp(config);
export const auth = firebase.auth;
export const db = firebase.database();
export const firestore = firebase.firestore();
const firebaseFunction = firebase.app().functions("europe-west1")
if (process.env.NEXT_PUBLIC_FIREBASE_EMULATOR_HOST)
  firebaseFunction.useEmulator(process.env.NEXT_PUBLIC_FIREBASE_EMULATOR_HOST, process.env.NEXT_PUBLIC_FIREBASE_EMULATOR_PORT ?? 5001)
export const functions = {
  createStripeSession: firebaseFunction.httpsCallable("createStripeSession"),
  setAdmin: firebaseFunction.httpsCallable("setAdmin"),
  azure: {
    SetupNewStreamingSession: firebaseFunction.httpsCallable("Azure-SetupNewStreamingSession", { timeout: 9 * 60 * 1000 }),
    StartStreamingSession: firebaseFunction.httpsCallable("Azure-StartStreamingSession", { timeout: 9 * 60 * 1000 }),
    StopAndDeleteStreamingSession: firebaseFunction.httpsCallable("Azure-ScheduleStopAndDeleteStreamingSession", { timeout: 9 * 60 * 1000 }),
    GetPlayToken: firebaseFunction.httpsCallable("Azure-GetPlayToken"),
    GetPlayLink: firebaseFunction.httpsCallable("Azure-GetPlayLink"),
  },
  getUsers: firebaseFunction.httpsCallable("getUsers"),
  setAdmin: firebaseFunction.httpsCallable("setAdmin"),
  deleteUser: firebaseFunction.httpsCallable("deleteUser"),
}