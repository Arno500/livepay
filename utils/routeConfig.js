const routes = {
  "/library": {
    signedIn: true,
  },
  "/live": {
    signedIn: true,
  },
  "/purchase": {
    signedIn: true
  },
  "/streams": {
    admin: true,
    signedIn: true
  },
  "/users": {
    admin: true,
    signedIn: true
  },
  "/products": {
    admin: true,
    signedIn: true
  },
  "/": {},
}

export default routes

export const getRouteConfig = (route) => {
  return routes[Object.keys(routes).find(path => route.match(path))]
}