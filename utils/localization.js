import i18n from 'i18next'
// import LanguageDetector from "i18next-browser-languagedetector"
import { initReactI18next } from 'react-i18next'
import languageEN from './locales/en'
import languageFR from './locales/fr'
import languageDE from './locales/de'

import dayjs from "dayjs"
import dayjsDuration from "dayjs/plugin/duration"
import dayjsRelativeTime from "dayjs/plugin/relativeTime"
import "dayjs/locale/fr"
import "dayjs/locale/de"
import "dayjs/locale/en"

import antdFR from 'antd/lib/locale/fr_FR';
import antdDE from 'antd/lib/locale/de_DE';
import antdEN from 'antd/lib/locale/en_US';

i18n
  // .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    resources: {
      en: languageEN,
      de: languageDE,
      fr: languageFR
    },
    // lng: "de",
    fallbackLng: "en",
    lowerCaseLng: true,
    load: "languageOnly",
    debug: false,
    ns: ["translations"],
    defaultNS: "translations",
    keySeparator: ".",
    interpolation: {
      escapeValue: false,
      formatSeparator: ","
    },
    react: {
      wait: true,
      bindI18n: 'languageChanged loaded',
      bindStore: 'added removed',
      nsMode: 'default'
    },
    saveMissing: true,
    saveMissingTo: "all"
  })

// const dayJSLocales = {
//   fr: dayjsFR,
//   en: dayjsEN,
//   de: dayjsDE,
// }

dayjs.extend(dayjsDuration)
dayjs.extend(dayjsRelativeTime)
// dayjs.locale(i18n.language.split("-")[0])
// console.log(dayJSLocales[i18n.language.split("-")[0]])
// console.log(dayJSLocales)

export const antdIntlMap = {
  fr: antdFR,
  en: antdEN,
  de: antdDE
}

export default i18n;

export const t = i18n.t.bind(i18n)

export const currencyFormatter = new Intl.NumberFormat(undefined, {
  style: "currency",
  currency: "EUR"
})
export const dateFormatter = new Intl.DateTimeFormat(undefined, {
  weekday: "short",
  year: "numeric",
  month: "short",
  day: "numeric",
  hour: "2-digit",
  minute: "2-digit"
})

export const relativeTimeFormatter = new Intl.RelativeTimeFormat(undefined, {
  localeMatcher: "best fit",
  numeric: "always",
  style: "long",
});